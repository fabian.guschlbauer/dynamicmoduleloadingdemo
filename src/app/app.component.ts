import {
  Component,
  ComponentFactoryResolver,
  ComponentRef,
  Injector,
  NgModuleRef,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {DynamicComponent} from './components/dynamic/dynamic.component';
import {DynamicService} from './services/dynamic.service';
import {DynamicModuleLoaderService} from './services/dynamic-module-loader.service';
import {DynamicModuleComponent} from './components/dynamic-module/dynamic-module.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'DynamicAngularExample';

  @ViewChild('dynamicContainer', {read: ViewContainerRef})
  dynamicContainer: ViewContainerRef;

  @ViewChild('dynamicModuleContainer', {read: ViewContainerRef})
  dynamicModuleContainer: ViewContainerRef;

  private _dynamicInstanceRefs: ComponentRef<DynamicComponent>[] = [];
  private _lazyLoadedService: DynamicService;

  private _moduleLoader: DynamicModuleLoaderService;
  private _lazyLoadedModule: NgModuleRef<any>;

  constructor(private componentFactoryResolver: ComponentFactoryResolver,
              private injector: Injector) {
  }

  async loadComponentAsync() {
    const {DynamicComponent} = await import('./components/dynamic/dynamic.component');
    const dynamicComponentFactory = this.componentFactoryResolver.resolveComponentFactory(DynamicComponent);
    this._dynamicInstanceRefs.push(
      this.dynamicContainer.createComponent(dynamicComponentFactory, null, this.injector));
  }

  destroyAllComponents() {
    this._dynamicInstanceRefs.forEach(component => {
      component.destroy();
    })
  }

  lazyLoadService() {
    this._lazyLoadedService = this.injector.get(DynamicService);
  }

  async lazyLoadModule() {
    if (!this._moduleLoader) {
      this._moduleLoader = this.injector.get(DynamicModuleLoaderService);
    }

    this._lazyLoadedModule =
      await this._moduleLoader.loadModule(import('./components/dynamic-module/dynamic.module')
        .then(m => m.DynamicModule));

    // lazy load component of module (can also be done inside the module. e.g exported components using static references)
    const dynamicModuleComponentFactory = this._lazyLoadedModule.componentFactoryResolver.resolveComponentFactory(DynamicModuleComponent);
    this._dynamicInstanceRefs.push(
      this.dynamicModuleContainer.createComponent(dynamicModuleComponentFactory, null));
  }
}
