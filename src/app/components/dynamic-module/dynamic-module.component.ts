import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-dynamic-module',
  templateUrl: './dynamic-module.component.html',
  styleUrls: ['./dynamic-module.component.sass']
})
export class DynamicModuleComponent implements OnInit {


  ngOnInit(): void {
    console.log("Dynamic Module Component created!")
  }

}
