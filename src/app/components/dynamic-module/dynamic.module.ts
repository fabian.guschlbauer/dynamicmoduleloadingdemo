import {NgModule, ViewChild, ViewContainerRef} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DynamicModuleComponent} from './dynamic-module.component';


@NgModule({
  declarations: [
    DynamicModuleComponent],
  exports: [
    DynamicModuleComponent
  ],
  imports: [
    CommonModule
  ]
})
export class DynamicModule {

  @ViewChild('dynamicModuleContainer', {read: ViewContainerRef})
  dynamicModuleContainer: ViewContainerRef;

  constructor() {
    console.log('Dynamic Module loaded!');
  }
}
