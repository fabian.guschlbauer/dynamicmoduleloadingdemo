import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dynamic',
  templateUrl: './dynamic.component.html',
  styleUrls: ['./dynamic.component.sass']
})
export class DynamicComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    console.log('Dynamic Component is loaded!');
  }

}
