import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DynamicService {

  constructor() {
    console.log('Dynamic Service is loaded!');
  }
}
